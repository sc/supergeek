# SUPERGEEK

A simple R script to change the colors stored in the pdf. Used to color the pdf made by the BSC available [here](https://www.bsc.es/sites/default/files/public/bscw2/pages/discover-bsc/supergeek-color.pdf)

As the pdf is not the best thing to manipulate the image we first convert it in EPS using `inkscape`:
```bash
wget https://www.bsc.es/sites/default/files/public/bscw2/pages/discover-bsc/supergeek-color.pdf
inkscape supergeek-color.pdf --export-eps=supergeek.eps #note that it could have been done using svg too, and with svg maybe more fancy stuff could have been done
```
then  just running 
```bash
Rscript supergeek.R
```

should gives you randomly coloured supergeek in `coloredsupergeek.eps`

to run with mpi:

```bash
mpirun -n 1 Rscript supergeek-mpi.R
```
The number of CPU used is hardcode in `supergeek-mpi.R`

to run with mpi in MN4A:

```bash
sbatch mn4job.job
```

try to change  `rainbow` in
```R
colorset=col2rgb(rainbow(ns))/255 #get some colors and convert them in rgb which is what EPS understand
```
by `heat.colors` or `topo.colors` ....

